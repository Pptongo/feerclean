<?php

namespace App\Extras;

use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;

/**
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @version v1.0.0
 * @since v1.0.0
 * @access public
 * @category Extras
 * @package App\Extras
 * @copyright Copyright (c) 2019, Feerclean
 * 
 * Esta clase contiene una serie de metodos y funciones genéricas para el uso en toda la aplicación.
 */
class Utils {

    /**
     * @method getResponse Se usa para convertir un Object y Boolean en un response para la aplicación web.
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @static
     * @param Array $response Arreglo de los mensajes y/o objetos que serán enviados como respuesta.
     * @param Boolean $success Se usa para indicar al Front-End sobre el resultado de una petición web.
     * @return Array Retorna el response generado para enviarse vía web.
     */
    public static function getResponse($response, $success = false) {
        return [
            'success' => $success,
            'body' => $response
        ];
    }

    /**
     * @method saveLog Método utilizado para guardar un registro de alguna excepción o problema durante la ejecución de la aplicación.
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @static
     * @param String $category Categoría de donde se originó el problema.
     * @param Array $errors Lista con descripciones de los errores ocurridos.
     * @param Number $type El tipo de LOG que se va a registrar.
     */
    public static function saveLog($category, $errors, $type = Logger::INFO) {
        $userInfo = '';

        if(Auth::guard('user_login')->check()) $userInfo = ' ID-USER: '.Auth::guard('user_login')->user()->user_id;

        switch($type) {
            case Logger::INFO:
                Log::channel($category)->info($errors.$userInfo);
                break;
            case Logger::ERROR:
                Log::channel($category)->error($errors.$userInfo);
                break;
            case Logger::WARNING:
                Log::channel($category)->warning($errors.$userInfo);
                break;
            case Logger::DEBUG:
                Log::channel($category)->debug($errors.$userInfo);
                break;
            case Logger::ALERT:
                Log::channel($category)->alert($errors.$userInfo);
                break;
            default:
                Log::channel($category)->notice($errors.$userInfo);
                break;
        }
    }

    /**
     * @method getObjectFromJSON Función para convertir JSON en un Objeto de PHP
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @static
     * @param  Array $json
     * @return Object  Objecto generado
     */
    public static function getObjectFromJSON($json) {
        return (object) $json;
    }

    /**
     * @method getRandPassword Función que generá una contraseña de forma aleatoria
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @static
     * @return String
     */
    public static function getRandPassword() {
        $chars = 'abcdefghijklmnsopqrstuvwxz';
        $nums = '1234567890';
        $specialChars = '@!=#?$%*';
        
        $password = strtoupper($chars[mt_rand(0, strlen($chars) - 1)]).$nums[mt_rand(0, strlen($nums) - 1)].$specialChars[mt_rand(0, strlen($specialChars) - 1)];

        while(strlen($password) < 16) {
            $password .= ($chars.$nums.$specialChars)[mt_rand(0, strlen($chars.$nums.$specialChars) - 1)];
        }

        return $password;
    }

}