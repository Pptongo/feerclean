<?php

namespace App\Http\Controllers\Auth;

use Validator;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Models\User;

use App\Extras\Utils;

class LoginController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * @method guard Se usa para indicar el Guard con el que se valida la sesión del usuario.
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @return Auth
     */
    protected function guard() {
        return Auth::guard('user_login');
    }
    
    public function login(Request $request) {
        $success = false;
        $response = [];
        $obj = $request->all();

        if($request->isJson()) {
            $valid = $this->valLoginForm($obj);

            if(!$valid->fails()) {
                $obj = (Object) $obj;
                $user = User::where('email', $obj->email)->first();

                if(isset($user)) {
                    if($user->email_verified_at != null) {
                        if(Auth::guard('web')->attempt(['email' => $obj->email, 'password' => $obj->password], false)) {
                            $success = true;
                        } else {
                            $response = ['message' => \Lang::get('errors.errorSession')];
                        }
                    } else {
                        $response = ['message' => \Lang::get('errors.emailNotVerified')];
                    }
                } else {
                    $response = ['message' => \Lang::get('errors.userNotFound')];
                }
            } else {
                $response = ['message' => $valid->errors()];
            }
        } else {
            $response = ['message' => \Lang::get('jsonFormat')];
        }

        return response()->json(Utils::getResponse($response, $success));
    }

    /**
     * @method valLoginForm Valida los campos enviados para inicio de sesión.
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param Array $data El cuerpo de la solicitud.
     * @return Validator
     */
    public function valLoginForm(Array $data) {
        return Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required',
            'keepSession' => 'sometimes|boolean'
        ]);
    }

}