<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Monolog\Logger;

use Validator;
use Auth;

use App\Models\LoginModel;
use App\Models\UserModel;

use App\Extras\Utils;

class LoginController extends Controller {

    use AuthenticatesUsers;

    protected function guard() {
        return Auth::guard('user_login');
    }

    public function login(Request $request) {
        $response = [];
        $success = false;

        try {
            if($request->isJson()) {
                $valid = $this->checkLoginRequest($request->all());
    
                if(!$valid->fails()) {
                    $data = (Object) $request->all();
                    $login = LoginModel::where('email', $data->email)->first();
    
                    if(isset($login)) {
                        $user = UserModel::where('id_login', $login->id)->first();
    
                        if(isset($user)) {
                            if($user->active) {
                                if($login->confirmed) {
                                    if(Auth::guard('user_login')->attempt(['email' => $data->email, 'password' => $data->password], false)) {
                                        $success = true;
                                    } else {
                                        $response = ['message' => \Lang::get('errors.login')];
                                    }
                                } else {
                                    $response = ['message' => \Lang::get('errors.confirmAccount')];
                                }
                            } else {
                                $response = ['message' => \Lang::get('errors.inactiveAccount')];
                            }
                        } else {
                            $response = ['message' => \Lang::get('errors.userNotExists')];
                        }
                    } else {
                        $response = ['message' => \Lang::get('errors.userNotExists')];
                    }
                } else {
                    $response = ['message' => $valid->errors()];
                }
            } else {
                $response = ['message' => \Lang::get('errors.jsonFormat')];
            }
        } catch(\Illuminate\Database\QueryException $e) {
            Utils::saveLog('database', $e->getMessage(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        } catch(\Exception $e) {
            Utils::saveLog('backend', $e->getTraceAsString(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        }

        return response()->json(Utils::getResponse($response, $success));
    }

    public function logout(Request $request) {
        Auth::guard('user_login')->logout();
        return redirect('/');
    }

    public function index() {
        session(['tmpProfile' => true]);
        session(['tmpRegister' => true]);

        return view('panel');
    }

    public function checkLoginRequest(Array $data) {
        return Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
    }

}