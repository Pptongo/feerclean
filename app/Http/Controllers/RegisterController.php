<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Crypt;

use App\Http\Controllers\Controller;


use App\Models\LoginModel;
use App\Models\UserModel;

use Monolog\Logger;

use Validator;
use Auth;

use App\Extras\Utils;

class RegisterController extends Controller
{
    /**
     * @method registerUser Método que recibe una petición para registrar un nuevo usuario
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param Request $request Solicitud del cliente
     * @return \Response
     */
    public function registerUser(Request $request) {
        $success = false;
        $response = [];

        try {
            if($request->isJson()) {
                $valid = $this->checkRegisterRequest($request->all());

                if(!$valid->fails()) {
                    $record = Utils::getObjectFromJSON($request->all());
                    $password = Utils::getRandPassword();

                    $login = LoginModel::create([
                        'email' => $record->email,
                        'password' => bcrypt($password),
                        'confirmation_token' => Crypt::encryptString($record->email)
                    ]);

                    if($login->id > 0) {
                        $user = UserModel::create([
                            'name' => $record->name,
                            'lastname' => $record->lastname,
                            'phone' => $record->phone,
                            'cellphone' => $record->mobile,
                            'id_login' => $login->id
                        ]);

                        $login->notifyRegister($user->name, $password);

                        $success = true;
                    }
                } else {
                    $response = ['message' => $valid->errors()];
                }
            } else {
                $response = ['message' => \Lang::get('errors.jsonFormat')];
            }
        } catch(\Illuminate\Database\QueryException $e) {
            Utils::saveLog('database', $e->getMessage(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        } catch(\Exception $e) {
            Utils::saveLog('backend', $e->getTraceAsString(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        }

        return response()->json(Utils::getResponse($response, $success));
    }

    /**
     * @method checkRegisterRequest Valida que los datos enviados para el registro de un usuario sean correctos
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param Array $data Lista con los datos a validar.
     * @return \Validator
     */
    private function checkRegisterRequest(Array $data) {
        return Validator::make($data, [
            'name' => 'required|string|min:3|max:50',
            'lastname' => 'required|string|min:3|max:50',
            'email' => 'required|email|confirmed|unique:logins,email',
            'phone' => 'sometimes|nullable|string',
            'mobile' => 'required|string|min:10'
        ]);
    }

    /**
     * @method confirmUser Valida y confirma un usuario registrado
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param Request $request Petición del cliente
     * @return Response
     */
    public function confirmUser(Request $request) {
        $success = false;
        $response = [];

        try {
            if(Auth::guard('user_login')->check()) {
                $path = '/panel';
                $query = $request->query();

                if(count($query) > 0) {
                    $path .= '?';

                    foreach($query as $key => $value) {
                        $path .= $key.'='.$value.'&';
                    }

                    $path = substr($path, 0, -1);
                }

                return redirect($path);
            }

            $decrypt = Crypt::decryptString($request->token);
            $login = LoginModel::where(['email' => $decrypt, 'confirmation_token' => $request->token])->first();

            if($login && Auth::guard('user_login')->loginUsingId($login->id)) {
                $login->confirmation_token = null;
                $login->confirmed = true;
                $login->save();

                return redirect()->action('LoginController@index');
            }
        } catch(\Illuminate\Database\QueryException $e) {
            Utils::saveLog('database', $e->getMessage(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        } catch(\Exception $e) {
            Utils::saveLog('backend', $e->getTraceAsString(), Logger::ERROR);
            $response = ['message' => \Lang::get('errors.internalServerError')];
        }

        return response()->json(Utils::getResponse($response, $success));
    }

}
