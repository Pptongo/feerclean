<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;

class ValidateLoginSession {

    public function handle($request, Closure $next) {
        if(!Auth::guard('user_login')->check()) {
            $request->session()->flush();
            return redirect('/');
        }

        return $next($request);
    }

}
