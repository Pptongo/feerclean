<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Notifications\NotificationEmail;

class LoginModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'logins';

    protected $fillable = [
        'email',
        'password',
        'remember_token',
        'confirmation_token',
        'confirmed'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    public function notifyRegister($name, $password) {
        $notify = new NotificationEmail(\Lang::get('general.notifyConfirmAccount'), [\Lang::get('general.notifyConfirmL1'), \Lang::get('general.notifyConfirmL2').$password], $name, '/users/confirmRegister/'.$this->confirmation_token, \Lang::get('general.notifyConfirmButton'));
        $this->notify($notify);
    }

}
