<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastname',
        'gender',
        'phone',
        'cellphone',
        'street',
        'no',
        'no_ext',
        'colony',
        'cp',
        'city',
        'state',
        'country',
        'role',
        'id_login',
        'active'
    ];
    
}
