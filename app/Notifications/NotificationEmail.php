<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @version v1.0.0
 * @package v1.0.0
 * @access public
 * @category Notification
 * @package App\Notifications
 * @copyright Copyright (c) 2019, Feerclean
 * 
 * Clase para la adminsitración de envío de correos
 */
class NotificationEmail extends Notification {
    use Queueable;

    /**
     * @var Array $bodyEmail Contiene un arreglo con las líneas de texto que se incluirán en el correo
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public $bodyEmail = [];

    /**
     * @var String $name El nombre del usuario al que se le envía la notificación
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public $name = '';

    /**
     * @var String $subject El título con el que llegará el correo al usuario
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public $subject = '';

    /**
     * @var String $url La URL que se usará para cuando el usuario haga clic en el botón de acción
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public $url = '';
    
    /**
     * @var String $buttonText El texto que se mostrrá en el botón de acción
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public $buttonText = '';

    /**
     * @var Array $cc Lista de correos a los que debe de enviarse una Copia
     * @author José Luis Pérez Olvera <jose.perez@maspm.lat>
     * @access public
     * @since v0.4b
     */
    public $cc = [];

    /**
     * @var Array $bcc Lista de correos a los que debe de enviarse una Copia oculta
     * @author José Luis Pérez Olvera <jose.perez@maspm.lat>
     * @access public
     * @since v0.4b
     */
    public $bcc = [];

    /**
     * @var Array $attach Lista de rutas de archivos para adjuntar al correo
     * @author José Luis Pérez Olvera <jose.perez@maspm.lat>
     * @access public
     * @since v0.4b
     */
    public $attach = [];

    /**
     * @method __construct Crea una instancia nueva para las notificaciones
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param String $subject El título del correo
     * @param Array $bodyEmail El arreglo con las líneas que se incluirán al correo
     * @param String $name El nombre del usuario que recibirá la notificación
     * @param String $url La URL que se usará en el link del correo y como acción del correo
     * @param String $buttonText El texto que se muestrá en el botón de acción
     */
    public function __construct($subject, $bodyEmail, $name, $url, $buttonText) {
        $this->subject =  $subject;
        $this->bodyEmail = $bodyEmail;
        $this->name = $name;
        $this->url = $url;
        $this->buttonText = $buttonText;
    }

    /**
     * @method via Obtiene el canal de comunicación para las notificaciones
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param \Mixed  $notifiable
     * @return Array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * @method toMail Obtiene el correo que se enviara como notificación
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @version v1.1
     * @param \Mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        $email = new MailMessage();
        $email->subject($this->subject);
        $email->greeting('Hola! '. $this->name);
        $email->cc($this->cc);
        $email->bcc($this->bcc);

        if(!empty($this->attach)) {
            foreach($this->attach as $attach) {
                $email->attach($attach);
            }
        }

        for($i = 0; $i < count($this->bodyEmail); $i++) {
            $email->line($this->bodyEmail[$i]);
        }

        if($this->url != '') $email->action($this->buttonText, url($this->url));

        return $email;
    }

    /**
     * @method toArray Obtiene una colección para representar la notificación
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     * @param Mixed $notifiable
     * @return Array
     */
    public function toArray($notifiable) {
        return [
            
        ];
    }

}