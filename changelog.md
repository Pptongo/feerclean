# Changelog
Todos los cambios significantes del proyecto estarán documentados en este documento.

## Instrucciones
<details><summary>
Para el uso correcto de este documento se debe seguir las siguientes indicaciones:</summary>

Al realizar un lanzamiento de la aplicación se deberá agregar un título con el número de versión entre corchetes seguido de la fecha de liberación, ejemplo:

> ## [1.0.0] - 2019-04-16

Por cada versión liberada se deberán especificar los cambios de la versión usando las siguiente clasificación:

> |Clasificación|Descripción|
> |-------------|-----------|
> |Agregado/Added|Especifica la lista de nuevas funciones|
> |Cambios/Changed|Especifica los cambios realizados en funciones ya existentes|
> |Obsoleto/Deprecated|Indica que funciones serán removidas en versiones futuras|
> |Eliminado/Removed|Funcionalidades que ya no están presentes|
> |Solucionado/Fixed|Para errores/bugs reportados en versiones anteriores|
> |Seguridad/Security|Solución a problemas de vulnerabilidades|

Ejemplo:

> ## [1.0.1] - 2019-04-16
> ### Agregado
> - Nueva función para imprimir reportes
> ### Cambios
> - La función exportar ahora soporta multiples archivos
> ### Obsoleto
> - Configuración de Reportes desde configuración. Nueva función en Reportes
> ### Eliminado
> - Se quito la función para exportar PDFs
> ### Solucionado
> - Solución a un problema con la carga de datos iniciales
> ### Seguridad
> - Solución a un problema que permitia visualizar reportes a un usuario sin permisos

Las notas de la versión deberán ser en ordén descendete mostrando primero la versión más actual.
</details>

---