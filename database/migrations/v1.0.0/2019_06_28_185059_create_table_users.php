<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('lastname', 100);
            $table->integer('gender')->default(0);
            $table->string('phone', 15)->nullable();
            $table->string('cellphone', 15)->nullable();
            $table->string('street', 100)->nullable();
            $table->string('no-int', 10)->nullable();
            $table->string('no_ext', 10)->nullable();
            $table->string('colony', 100)->nullable();
            $table->string('cp', 5)->nullable();
            $table->integer('city')->nullable();
            $table->integer('state')->nullable();
            $table->integer('country')->nullable();
            $table->integer('role')->default(1);
            $table->bigInteger('id_login')->unsigned();
            $table->integer('active')->default(1);
            $table->timestamps();
            $table->foreign('id_login')->references('id')->on('logins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
