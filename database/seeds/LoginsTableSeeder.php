<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @version v1.0
 * @since v1.0.0
 * @access public
 * @category Seeds
 * @package database\seeds
 * @copyright Copyright (c) 2019, FEERCLEAN
 * 
 * Clase para agregar datos a la tabla de logins
 */
class LoginsTableSeeder extends Seeder {

    /**
     * @method run Se usa para ejecutar el Seed
     * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
     * @access public
     * @since v1.0.0
     */
    public function run() {
        DB::table('logins')->insert([
            'email' => 'admin@feerclean.com',
            'password' => bcrypt('L0k3rs!'),
            'confirmed' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'name' => 'José Luis',
            'lastname' => 'Pérez Olvera',
            'role' => 0,
            'country' => 1,
            'state' => 9,
            'city' => 16,
            'id_login' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

}