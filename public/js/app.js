function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * @file app.js
 * @description Este archivo contiene todas las funciones genéricas de la aplicación.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @copyright Copyright (c) 2019, Feerclean
 */

/**
 * @var
 * @name _modal
 * @description Una vista en forma de modal para presentar contenido adicional al usuario.
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
var _modal;
/**
 * @var
 * @name _view
 * @description Contiene el nombre de la vista actual, se usa para saber que vista refrescar.
 * @type {String}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * 
 */


var _view = '/';
/**
 * @var
 * @name _history
 * @description Se usa para guardar el historial de la navegación del usuario y poder moverse entre las diferentes vistas cargadas.
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

var _history = {
  position: 0,
  views: []
};
/**
 * @constant
 * @name NOTIFICATION_TYPE
 * @description Define los diferentes tipos de notificación que se pueden utilizar.
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

var NOTIFICATION_TYPE = {
  SUCCESS: 'success',
  ERROR: 'error',
  WARNING: 'warning',
  INFO: 'info'
};
/**
 * @function
 * @name DOM.ready
 * @description Inicializa todos los servicios un funciones básicos de la aplicación.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

$(document).ready(function () {
  setMainHeight();
  $('body').fadeIn(500);
});
/**
 * @function
 * @name Window.resize
 * @description Ejecuta funciones necesarias para mantener el aspecto de algunos controles, después de que la ventana principal sufra un cambio de dimensiones.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

$(window).resize(function () {
  setMainHeight();
});
/**
 * @function
 * @description Esta función es usada para generar todos los listeners que se requieren en la aplicación para cada evento que se genera.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

$(function () {
  $('[data-smooth-scroll]').click(function () {
    var target = this.hash;
    $('html, body').stop().animate({
      scrollTop: $(target).offset().top - 100
    }, 500, 'swing', function () {
      window.location.hash = target;
    });
  });
});
/**
 * @function
 * @name setMainHeight
 * @description Ajusta el tamaño de los elementos HTML y Body a la resolución de la ventana del navegador.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */

function setMainHeight() {
  var height = $(window).height() - ($('#mainNav').length > 0 ? 17 : 0);
  $('html body').height("".concat(height, "px"));
  $('#sidemenu').height(height - 25);
}
/**
 * @function
 * @name notify
 * @description Muestra una notificación al usuario.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {String} message El mensaje que será mostrado dentro de la notificación.
 * @param {String} title El título que se mostrará en la notificación.
 * @param {String} time El tiempo que lleva presente la notificación.
 * @param {String} type Indica el estilo con el cuál se mostrará la notificación.
 * @param {String} position Inidica la posición en donde deberá mostrarse la notificación al usuario.
 * @param {Number} duration Asigna el tiempo que la notificación deberá estar presente antes de desvanecerse.
 */


function notify(message) {
  var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : NOTIFICATION_TYPE.SUCCESS;
  var duration = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 3000;
  var icon = '';

  switch (type) {
    case NOTIFICATION_TYPE.SUCCESS:
      icon = 'fa-check-circle';
      toastr.success(message, title, {
        timeOut: duration
      });
      break;

    case NOTIFICATION_TYPE.ERROR:
      icon = 'fa-times-circle';
      toastr.error(message, title, {
        timeOut: duration
      });
      break;

    case NOTIFICATION_TYPE.WARNING:
      icon = 'fa-exclamation-triangle';
      toastr.warning(message, title, {
        timeOut: duration
      });
      break;

    case NOTIFICATION_TYPE.INFO:
      icon = 'fa-exclamation-circle';
      toastr.info(message, title, {
        timeOut: duration
      });
      break;
  }
}
/**
 * @function
 * @name displayLoading
 * @description Carga una vista con un indicador de carga para indicar al usuario que se esta realizando un operación y que debe esperar antes de continuar.
 * @author José Luis Pérez Olvera <siste_pp@hotmail.com>
 * @since v1.1.0
 * @param {String} additionalContent Se usa para casos en donde se quierar mostrar información adicional justo debajo del indiicador de carga.
 */


function displayLoading() {
  var additionalContent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  hideLoading();
  $('#loadingAdditionalContent').html(additionalContent);
  $('#loading').attr('style', 'display: block;');
}
/**
 * @function
 * @name hideLoading
 * @description Oculta la vista de carga.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.1.0
 */


function hideLoading() {
  $('#loading').attr('style', 'display: none;');
  $('#loadingAdditionalContent').html('');
}
/**
 * @function
 * @name getValueKeyWithDefault
 * @description Búsca dentro de un objeto la llave especificada, si no la encuentra regresa el valor que se envío por defecto.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {Object} obj El objeto que contiene la llave solicitada.
 * @param {String} key La llave o propiedad del objeto que se esta buscando.
 * @param {Object} defValue El valor por defecto que se retornara si no se encontró la llave.
 */


function getValueKeyWithDefault(obj, key) {
  var defValue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  if (obj != null && _typeof(obj) === 'object') {
    var val = obj[key];

    try {
      val = val.trim();
    } catch (e) {}

    return val !== undefined && val != "null" && val != '' ? val : defValue;
  }

  return defValue;
}
/**
 * @function
 * @name login
 * @description Función que se ejecuta si la validación del usuario fue correcta
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {Object} response La respuesta que se realizó desde el servidor.
 */


function login(response) {
  if (response != null && _typeof(response) === 'object') {
    if (response.success) {
      window.location.href = '/';
    }
  }
}
/**
 * @function
 * @name register
 * @description Función que se ejecuta después de enviar una solicitud de registro.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {Object} response La respuesta que se recibe desde el servidor.
 */


function register(response) {
  if (response != null && _typeof(response) === 'object') {
    try {
      if (response.success) {
        alert('Se ha realizado el registro con éxito, pronto recibirá un correo para confirmar su cuenta. Si no recibe el correo verifique su buzón de correos no deseados.');
      } else {
        alert(getErrorFromResponse(response.body.message));
      }
    } catch (e) {
      console.error(e);
      alert('Hubo un problema con el registro, intente de nuevo.');
    }
  }
}
/**
 * @function
 * @name getErrorFromResponse
 * @description Valida si el mensaje obtenido del servidor es un String o un Array y muestra los mensajes de acuerdo al resultado
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {Object} message La respuesta enviada dentro del response
 * @return {String} Retorna la respuesta o primer respuesta encontrada
 */


function getErrorFromResponse(message) {
  if (message != null) {
    return typeof message === 'string' ? message : message[0];
  }

  return '';
}
