# FEERCLEAN Lockers
#### Sistema para el control de servicios de lockers

## PRE-REQUISITOS
Para generar un ambiente de desarrollo del proyecto, es necesario tener instaldo los siguientes elementos en el equipo:

- [NodeJS >= v10.5.0](https://nodejs.org/es/) *
- [Composer >= v1.7.2](https://getcomposer.org/) *
- [PHP >= 7.1.3](http://www.php.net/) *
- [GIT >= 2.18.0](https://git-scm.com/) *
- [Visual Studio Code](https://code.visualstudio.com/)
- [XDebug](https://xdebug.org/wizard.php)

_NOTA: Los requisitos con un asterisco `*` son obligatorios para la implementación del ambiente._

**_PHP tiene que estar configurado en las variables de entorno para poder ejecutar comandos desde Terminal o CMD._**

## Configuración del ambiente

Para configurar el ambiente de desarrollo es necesario abrir una Terminal o CMD y ejecutar los comandos siguientes.

- Clonar el repositorio del proyecto usando GIT: 

    `git clone https://gitlab.com/Pptongo/feerclean.git`

- Instalar las dependencias del proyecto con Composer:

    `composer install`

- Instalar las dependencias de NodeJS:

    `npm install`

- Crear el archivo de configuración de entorno del proyecto. Para esto es necesario copiar el archivo **.env.example** y renombrarlo como **.env**
- Definir las variables en el archivo **.env** con los valores para el entorno **local**.
- Crear la llave de encriptación para el ambiente de desarrollo con el siguiente comando:

    `php artisan key:generate`

- Asignar permisos de escritura completa a la carpeta de **Storage** para permitir la escritura de logs y archivos del sistema.
- Crear el symlink de la carpeta **Storage** en public para poder acceder a los archivos publicados:

    `php artisan storage:link`

- Iniciar el servidor **Homebrew** de **Laravel** para el desarrollo con el comando:

    `php artisan serve`

- Ir al navegador y escribir:

    `http://localhost:8000`


### Configuración adicional en IIS

Si la aplicación se monta en un servidor IIS es necesario tener instalado el módulo [Microsoft URL Rewrite Module 2.0 for IIS](https://www.microsoft.com/en-us/download/details.aspx?id=7435).

Dentro de la carpeta **Public** ya viene un archivo **web.config** con las reglas configuradas para el redireccionamiento de la aplicación.

## Reglas para el control de versiones

El control de versiones de la aplicación se hace por medio de **_GIT_**.

Para el control de la base de datos se hace a través de archivos llamadas _migrations_ dentro de **_/database/migrations/'version'_** y por cada versión se agrega la carpeta de la versión.

### Crear Migrations

Los migrations son archivos que se usan para crear estructuras de datos en la Base de Datos.

Para crear un migration es necesario usar el comando:

`php artisan make:migration --path="database/migrations/'version'/" NombreDelMigrate`

### Usar los Migrations

Para ejecutar un migration y afectar a la Base de Datos se usa el comando:

`php artisan migrate --path="database/migration/'version'/"`

### Rollback de la BD

Si durante la ejecución de un migration se necesita hacer un rollback por algún error, es necesario usar el comando:

`php artisan migrate:rollback --path="database/migration/'version'/"`

## Crear Modelos
Los modelos son objetos que se usan para adminsitrar las tablas de la Base de Datos.

En cada modelo debe indicarse la **tabla** a la que hace referencia, las **columnas** que son manipulables y las que no deben visualizarse. Así como todas las funciones y metodos que requiera para manipularlo.

Para crear un nuevo modelo se hace desde Terminal o CMD con el comando:

`php artisan make:model Models/carpeta/NombreDelModeloTerminandoEnModel`

## Uso de REST

La aplicación se divide en dos capas principales, Fron-End y Back-End. La comunicación entre las capas es a través de **WebServices** por medio de **REST**.

Para usar **REST** desde el Front-End se hace a través de peticiones web por medio de **AJAX** y usando los metodos **GET**, **POST**, **PUT**, **DELETE**.

Todas las peticiones que se efectuen deben contener el **_token_** de sesión del usuario conocido como **X-CSFR-TOKEN**.

La estructura de una petición **AJAX** debe contener la siguiente cabecera:

`headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},`

### Respuestas del servidor

La respuesta que se obtiene del servidor a través de **AJAX** llevá la misma estructura para todo servicio:

`{`
&nbsp;&nbsp;&nbsp;&nbsp;`"success": <boolean>,`
&nbsp;&nbsp;&nbsp;&nbsp;`"response": {`
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`// El contenido dependerá del servicio`
&nbsp;&nbsp;&nbsp;&nbsp;`}`
`}`

## Uso de Sass

El desarrollo de la aplicación utiliza [Sass](https://sass-lang.com/guide) para la copilación de estilos CSS mejorando así el desarrollo de los estilos.

Cada que se realiza un cambio en Sass es necesario copilar para que se creen los estilos CSS que usa la aplicación y poder ver los cambios reflejados.

Para copilar en desarrollo se usa el comando `npm run dev`. Esto genera los archivos CSS en `public/css`.

Si se requiere copilar los archivos Sass para producción se usa el comando `npm run prod` y esto generará los archivos CSS minificados para su uso en ambientes de producción.