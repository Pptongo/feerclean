/**
 * @file forms.js
 * @description Este archivo contiene las funciones para el manejo de formularios y sus elementos.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @copyright Copyright (c) 2019, Feerclean
 */

/**
 * @var
 * @name _files
 * @description Contiene la lista de archivos que serán cargados al servidor.
 * @since v1.0.0
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 */
let _files = [];

/**
 * @constant
 * @name VAL_RULES
 * @description Describe the rules used for validate fields and letiables
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
const VAL_RULES = {
    name: {
        minLong: 3,
        maxLong: 50,
        validate: (val) => !(/^([a-zñáéíóúÑÁÉÍÓÚ]+[\s]*)+$/).test(val.toLowerCase())
    },
    alfanumeric: {
        minLong: 1,
        maxLong: 255,
        validate: (val) => !(/^([\w]*[\s]*[a-zñáéíóúÑÁÉÍÓÚ,;:$&|()?¿!¡¿"%@#+*'\<\>={}_°\./\\\-/]*)+$/).test(val)
    },
    number: {
        minLong: 1,
        maxLong: 50,
        validate: (val) => !(/^([0-9])*$/).test(val)
    },
    decimal: {
        minLong: 1,
        maxLong: 50,
        validate: (val) => !(/^([0-9]*[.])?[0-9]*$/).test(val)
    },
    date: {
        validate: (val) => !/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/.test(val)
    },
    time: {
        validate: (val) => !/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(val)
    },
    email: {
        minLong: 8,
        maxLong: 100,
        validate: (val) => !(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i).test(val)
    },
    phone: {
        minLong: 10,
        maxLong: 12
    },
    url: {
        minlong: 10,
        maxLong: 255,
        validate: (val) => !(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]$/g).test(val)
    },
    list: {}
};

/**
 * @function
 * @description Inicializa los listeners necesarios para los elementos web.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
$(function() {
    $('[data-frm-validate]').change(function() {
        cleanField(this);
        if(this.required || this.value !== '') valField(this);
    });

    $('[data-submit]').click(function() { submitFrm(this); });
})

/**
 * @function
 * @name disableForm
 * @description Deshabilíta cada elemento del formulario especificado.
 * @author José Luis Pérez Olvera <sistem_p@hotmail.com>
 * @since v1.0.0
 * @param {String} id El ID del formulario que será limpiado.
 */
function disableForm(id) {
    const frm = document.getElementById(id);
    if(frm !== undefined) for(let i = 0; i < frm.length; i++) frm[i].disabled = true;
}

/**
 * @function
 * @name validateForm
 * @description Revisa el valor de cada campo de un formulario y valida que su contenido sea correcto.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {String} id El ID del formulario que será evaluado.
 * @return {Boolean} El resultado de la validación
 */
function validateForm(id) {
    const frm = document.getElementById(id);
    let validation = false;

    if(frm !== undefined) {
        validation = true;
        cleanForm(id);

        for(let i = 0; i < frm.length; i++) {
            let e = frm[i];

            if($(e).attr('required') !== undefined || (($(e).attr('type') !== 'checkbox' && $(e).attr('type') !== 'radio') && e.value !== '')) {
                if(!valField(e)) {
                    validation = false;
                }
            }
        }
    }

    if(!validation) notify('Error al validar los datos del formulario');

    return validation;
}

/**
 * @function
 * @name valField
 * @description Valida el contenido de un campo de formulario con sus respectivas reglas.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será validado por las reglas.
 * @return {Boolean} Resultado de la validación.
 */
function valField(field) {
    switch($(field).data('frmValidate')) {
        case Object.keys(VAL_RULES)[0]:
            return valTypeName(field);
        case Object.keys(VAL_RULES)[1]:
            return valTypeAlfanumeric(field);
        case Object.keys(VAL_RULES)[2]:
            return valTypeNumeric(field);
        case Object.keys(VAL_RULES)[3]:
            return valTypeDecimal(field);
        case Object.keys(VAL_RULES)[4]:
            return valTypeDate(field);
        case Object.keys(VAL_RULES)[5]:
            return valTypeTime(field);
        case Object.keys(VAL_RULES)[6]:
            return valTypeEmail(field);
        case Object.keys(VAL_RULES)[7]:
            return valTypePhone(field);
        case Object.keys(VAL_RULES)[8]:
            return valTypeUrl(field);
        case Object.keys(VAL_RULES)[9]:
            return valTypeList(field);
        case 'none':
            return true;
        default:
            $(field).addClass('is-invalid');
            return false;
    }
}

/**
 * @function
 * @name valTypeName
 * @description Valida el valor de un campo tipo nombre.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.1.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la evaluación.
 */
function valTypeName(field) {
    if(field != null) {
        cleanField(field);

        const min = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'minLength' : 'min', VAL_RULES.name.minLong);
        const max = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'maxLength' : 'max', VAL_RULES.name.maxLong);

        if(field.value.length < min || field.value.length > max || VAL_RULES.name.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeList
 * @description Valida el valor de un campo tipo lista.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypeList(field) {
    if(field != null) {
        cleanField(field);

        if(field.value.trim() == '') {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeAlfanumeric
 * @description Valida el valor de un campo tipo alfanumérico.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypeAlfanumeric(field) {
    if(field != null) {
        cleanField(field);

        const min = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'minLength' : 'min', VAL_RULES.alfanumeric.minLong);
        const max = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'maxLength' : 'max', VAL_RULES.alfanumeric.maxLong);

        if(field.value.length < min || field.value.length > getValueKeyWithDefault(field, max, VAL_RULES.alfanumeric.maxLong) || VAL_RULES.alfanumeric.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeNumeric
 * @description Valida el valor de un campo tipo numérico.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypeNumeric(field) {
    if(field != null) {
        cleanField(field);

        const min = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'minLength' : 'min', VAL_RULES.numeric.minLong);
        const max = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'maxLength' : 'max', VAL_RULES.numeric.maxLong);

        if(field.value.length < min || field.value.length > max || VAL_RULES.numeric.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeDecimal
 * @description Valida el valor de un campo de tipo decimal.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación
 */
function valTypeDecimal(field) {
    if(field != field) {
        cleanField(field);

        if(field.value.length < VAL_RULES.float.minLong || field.value.length > VAL_RULES.float.maxLong || field.value.substr(0, 1) == '.' || field.value.substr(-1) == '.' || VAL_RULES.float.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeDate
 * @description Valida el valor de un campo de tipo fecha.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypeDate(field) {
    if(field != null) {
        cleanField(field);
        let maxDate = true;
        let minDate = true;

        if($(field).data('maxDate') !== undefined) {
            const opts = $(field).data('maxDate');
            const date = opts.id !== undefined ? $(`#${opts.id}`).val() : (opts.name !== undefined ? $(`[name="${opts.name}"]`).val() : opts.date);
            if(date != null && date != '') maxDate = field.value <= date;
        }

        if($(field).data('dateMin') !== undefined) {
            const opts = $(field).data('dateMin');
            const date = opts.id !== undefined ? $(`#${opts.id}`).val() : (opts.name !== undefined ? $(`[name="${opts.name}"]`).val() : opts.date);
            if(date != null && date != '') minDate = field.value >= date;
        }

        $('[data-max-date]').each(function() {
            if(field.name != this.name) {
                const data = $(this).data('appDateMax');
                if(data.id !== undefined && field.id == data.id && this.value.trim() != '') valTypeDate(this);
                if(data.name !== undefined && field.name == data.name && this.value.trim() != '') valTypeDate(this);
            }
        });

        $('[data-min-date]').each(function() {
            if(field.name != this.name) {
                const data = $(this).data('dateMin');
                if(data.id !== undefined && field.id == data.id && this.value.trim() != '') valTypeDate(this);
                if(data.name !== undefined && field.name == data.name && this.value.trim() != '') valTypeDate(this);
            }
        });

        if(VAL_RULES.date.validate(field.value) || !maxDate || !minDate) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeTime
 * @description Valida el valor de un campo de tipo tiempo.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} El resultado de la validación.
 */
function valTypeTime(field) {
    if(field != field) {
        cleanField(field);
        
        if(VAL_RULES.time.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeEmail
 * @description Valida el valor de un campo de tipo correo.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypeEmail(field) {
    if(field != null) {
        cleanField(field);

        const min = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'minLength' : 'min', VAL_RULES.email.minLong);
        const max = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'maxLength' : 'max', VAL_RULES.email.maxLong);

        if(field.value.length < min || field.value.length > max || VAL_RULES.email.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }
    
    return false;
}

/**
 * @function
 * @name valTypePhone
 * @description Valida el valor de un campo de tipo teléfono.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */
function valTypePhone(field) {
    if(field != null) {
        cleanField(field);

        const min = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'minLength' : 'min', VAL_RULES.phone.minLong);
        const max = getValueKeyWithDefault(field, field.tagName === 'TEXTAREA' ? 'maxLength' : 'max', VAL_RULES.phone.maxLong);

        if(field.value.length < min || field.value.length > max) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name valTypeUrl
 * @description Valida el valor de un campo de tipo URL
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será evaluado.
 * @return {Boolean} Resultado de la validación.
 */ 
function valTypeUrl(inp) {
    if(field != null) {
        cleanField(field);

        if(field.value.length < VAL_RULES.url.minLong || field.value.length > VAL_RULES.url.maxLong || VAL_RULES.url.validate(field.value)) {
            field.classList.add('is-invalid');
        } else {
            field.classList.add('is-valid');
            return true;
        }
    }

    return false;
}

/**
 * @function
 * @name cleanForm
 * @description Limpia el resultado de una validación hacía un formulario.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {String} id El ID del formulario que será limpiado.
 * @param {Boolean} removeValues Se usa para indicar si deberá eliminarse el valor de cada campo o solo los estilos de la validación.
 */
function cleanForm(id, removeValues = false) {
    const frm = document.getElementById(id);

    if(frm !== undefined) {
        for(let i = 0; i < frm.length; i++) {
            cleanField(frm[i], removeValues);
        }
    }
}

/**
 * @function
 * @name cleanField
 * @description Límpia los estilos y valores de un campo de cualquier tipo.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} field El campo que será limpiado.
 * @param {Boolean} removeValue Indica si deberá o no eliminarse el valor del campo.
 */
function cleanField(field, removeValue = false) {
    if(field != null && typeof field === 'object') {
        try {
            $(field).removeClass('is-invalid', 'is-valid');

            if(removeValue) $(field).val('');
        } catch(e) {
            console.error(e);
        }
    }
}

/**
 * @function
 * @name getFormData
 * @description Obtiene el valor de cada campo del formulario en un Object o String.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {String} id El ID del formulario del cual se quieren obtener los datos.
 * @param {String} method Indica el método para el cuál serán usados los datos. Esto se usa para retornar el resultado en Object o String.
 * @return {Object | String} El resultado puede ser un Object o un String, depende del método del servicio web.
 */
function getFormData(id, method = WS_METHOD.POST) {
    const frm = document.getElementById(id);
    let objReq = {};
    let strReq = '';

    if(frm !== undefined) {
        for(let i = 0; i < frm.length; i++) {
            const e = frm[i];
            const name = $(e).attr('name');
            const type = $(e).attr('type');

            if(name !== undefined && (e.nodeName === 'SELECT' || e.nodeName === 'TEXTAREA' || type !== undefined)) {
                if(method === WS_METHOD.POST) {
                    objReq[name] = (type === 'checkbox' || type === 'radio') ? e.checked : e.value.trim() === '' ? null : e.value.trim();
                } else {
                    strReq += (strReq.trim() == '' ? '' : '&') + `${name}=${(type === 'checkbox' || type === 'radio') ? e.checked : e.value.trim() === '' ? null : e.value}`;
                }
            }
        }

        $('[data-drop-files]').each(function() {
            if(_files != undefined) {
                objReq['files'] = _files;
            }
        })
    }

    return method === WS_METHOD.POST ? objReq : strReq;
}

/**
 * @function
 * @name submitFrm
 * @description Valida la información de un formulario y luego lo envía para su proceso.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @param {DOM} e El elemento web que inicio el evento
 */
function submitFrm(e) {
    if(e != null) {
        const id = $(e).data('submit');

        if(validateForm(id)) {
            const form = document.getElementById(id);
            const onSubmit = $(form).data('onsubmit');

            doRequest($(form).attr('action'), response => {
                try {
                    if(response.success) {
                        if(onSubmit !== undefined) {
                            (onSubmit.substring(0, 1) === '/') ? loadView(onSubmit) : window[onSubmit](response);
                        }
                    } else {
                        alert(getErrorFromResponse(response.body.message));
                    }
                } catch(e) {
                    console.error(e);
                }
            }, error => {
                console.error(error);
            }, getFormData(id), form.method.toUpperCase(), WS_CONTENT_TYPE.JSON, WS_DATA_TYPE.JSON);
        } else {
            alert('Error al validar el formulario');
        }
    }
}