/**
 * @file request.js
 * @description Este archivo se usa para el manejo de peticiones a servicios web con RESTFull.
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 * @copyright Copyright (c) 2019, Feerclean
 */

/**
 * @constant
 * @name WS_METHOD
 * @description Se usa para definir que tipo de método se utilizará al realizar la petición web.
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
const WS_METHOD = {
    POST: 'POST',
    GET: 'GET',
    DELETE: 'DELETE',
    PUT: 'PUT'
};

/**
 * @constant
 * @name WS_CONTENT_TYPE
 * @description Se usa para especificar el tipo de contenido que será enviado al servicio web
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
const WS_CONTENT_TYPE = {
    JSON: 'application/json',
    FORM: 'application/x-www-form-urlencoded',
    PDF: 'application/pdf',
    XML: 'application/xml',
    ZIP: 'application/zip',
    TEXT: 'text/plain'
};

/**
 * @constant
 * @name WS_DATA_TYPE
 * @description Se usa para indicar el tipo de contenido que se espera como respuesta de un servicio web.
 * @type {Object}
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 * @since v1.0.0
 */
const WS_DATA_TYPE = {
    JSON: 'json',
    HTML: 'html'
};

/**
 * @function
 * @name
 * @description Esta función sirve para agregar de forma automática a todas las solicitudes que se realizan a un servicio web, las cabeceras correctas para su autenticación.
 * @since v1.0.0
 * @author José Luis Pérez Olvera <sistem_pp@hotmail.com>
 */
$(function() {
    const token = $('meta[name="_csrf"]')[0].content;
    const header = 'X-CSRF-TOKEN';

    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
});

/**
 * @function
 * @name doRequest
 * @description Send a request to a REST web service
 * @since v1.0
 * @author Jose Luis Perez Olvera <jolvera6@slb.com>
 * @param {String} url La URL del servicio web al que se quiere realizar una petición
 * @param {Function} onSuccess Callback que se ejecuta una vez finalizada la petición web.
 * @param {Function} onError Callback a ejecutar cuando se generá una excepción con la petición.
 * @param {JSON | String} data El cuerpo de la petición que será enviado. Puede ser un string o un JSON.
 * @param {String} method El método que se usará para invocar al servicio web.
 * @param {String} contentType El tipo de contenido que será enviado a través de la petición web.
 * @param {String} dataType El tipo de contenido que se esperá recibir por parte de la solicitud.
 * @param {String} onBefore Callback que se ejecuta antes de comenzar a realizar la petición web.
 * @param {String} showLoading Indica si debe mostrarse la vista de cargando o no.
 */
function doRequest(url, onSuccess = null, onError = null, data = null, method = WS_METHOD.GET, contentType = WS_CONTENT_TYPE.JSON, dataType = WS_DATA_TYPE.JSON, onBefore = null, showLoading = true) {
    $.ajax({
        timeout: 0,
        url: url.substr(1),
        type: method,
        data: data != null ? (typeof data == 'object' ? JSON.stringify(data) : data) : null,
        dataType: dataType,
        contentType: contentType,
        cache: false,
        beforeSend: xhr => {
            if(showLoading) displayLoading();
            if(onBefore != null && typeof onBefore == 'function') onBefore(xhr);
        },
        success: response => {
            if(showLoading) hideLoading();
            if(onSuccess != null && typeof onSuccess == 'function') onSuccess(response);
        },
        error: (xhr, status, error) => {
            if(xhr.status != 403) if(onError != null && typeof onError == 'function') onError(error);
            console.error(error);
            notify(e);
            if(showLoading) hideLoading();
        },
        statusCode: {
            403: () => notify(messages.httpErrors[403]),
            302: () => notify(messages.httpErrors[302])
        }
    });
}