<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during errors for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    '503title' => 'Página en construcción',
    '503description' => 'La página se encuentra en construcción, registrate en el siguiente formulario para recibir más información acerca de nosotros.',
    'login' => 'Error al iniciar sesión.',
    'confirmAccount' => 'Aún no has confirmado tu cuenta, debes confirmarla antes de poder iniciar sesión.',
    'inactiveAccount' => 'Tu cuenta se encuentra deshabilitada, por favor contactanos para reactivar tu cuenta.',
    'userNotExists' => 'El usuario o la contraseña son incorrectos.',
    'jsonFormat' => 'Error, la solicitud no es válida.',
    'internalServerError' => 'Se generó un error interno con su solicitud, por favor intente más tarde.'

];
