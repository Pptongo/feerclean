<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during general for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // Others
    'lang' => 'es',
    'dir' => 'ltr',

    // Labels
    'lblTitle' => 'FEERCLEAN',
    'lblHome' => 'Inicio',
    'lblPanel' => 'Panel',
    'lblName' => 'Nombre',
    'lblEmail' => 'Correo',
    'lblSubscribe' => 'Subscribete a nuestro canal de noticias.',
    'lblEmailExample' => 'nombre@correo.com',
    'lblMenu' => 'Menu',
    'lblContact' => 'Contacto',
    
    // Buttons
    'btnRegister' => 'Registrarse',
    'btnLogin' => 'Iniciar Sesión',
    'btnLogout' => 'Cerrar Sesión',
    'btnSubscribe' => 'Subscribirse',

    // Menu
    'mnuHowItsWorks' => '¿Cómo funciona?',
    'mnuPricing' => 'Precios',
    'mnuLocations' => 'Unicaciones',
    'mnuFaq' => 'Preguntas frecuentes',
    'mnuContact' => 'Contacto',

    // Login
    'email' => 'Correo',
    'emailHelp' => 'Debes ingresar tu cuenta de correo que usaste para tu registro. Si aún no estas registrado puedes hacerlo fácilmente.',
    'password' => 'Contraseña',
    'forgotPassword' => 'He olvidado mi contraseña',

    // Register
    'lblRegister' => '¡Registrate en nuesta aplicación para que tengas acceso a nuestros servicios!',
    'lblLastname' => 'Apellidos',
    'lblEmailConfirm' => 'Confirmar correo',
    'lblPhone' => 'Teléfono',
    'lblMobile' => 'Celular'

];
