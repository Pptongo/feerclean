@extends('errors.layout')
@section('error', \Lang::get('errors.503title'))
@section('description', \Lang::get('errors.503description'))
@section('additionalContent')
    <div class="card text-left mt-5 app-register">
        <div class="card-body">
            <form>
                <div class="form-group">
                    <label for="strName">@lang('general.lblName')</label>
                    <input type="text" name="strName" class="form-control">
                </div>
                <div class="form-group">
                    <label for="strEmail">@lang('general.lblEmail')</label>
                    <input type="email" name="strEmail" class="form-control">
                </div>
                <button type="button" class="btn btn-success btn-block">@lang('general.btnRegister')</button>
            </form>
        </div>
    </div>
@endsection