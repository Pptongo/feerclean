@extends('layouts.master')
@section('title', \Lang::get('errors.503title'))
@section('content')
    <section class="row m-0 h-100 justify-content-center align-items-center app-bg-errors">
        <div class="text-center">
            <img src="/images/logo.png" class="img-fluid" width="375" height="152" alt="Logo">
            <h1>@yield('error')</h1>
            <p>@yield('description')</p>
            @yield('additionalContent')
        </div>
    </section>
@endsection