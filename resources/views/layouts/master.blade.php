<!DOCTYPE html>
<html lang="@lang('general.lang')" dir="@lang('general.dir')">
<head>
    <title>@lang('general.lblTitle') | @yield('title')</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1.0">
    <meta name="_csrf" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Import Fontawesome 5.9.0 -->
    <link rel="stylesheet" href="/libs/fontawesome-5.9.0/css/all.min.css" defer>

    <!-- Import Bootstrap 4.3.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" defer>

    <!-- Import DataTables Styles -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css" defer>

    <!-- Import Toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" defer>

    <link rel="stylesheet" href="{{ asset('css/app.css?v='.config('app.version')) }}" defer>
</head>
<body class="bg-light">
    
    @yield('nav')
    @yield('content')
    @yield('footer')

    <!-- Import JQuery 3.4.1 -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous" defer></script>

    <!-- Import Bootstrap 4.3.1 -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" defer></script>

    <!-- Import DataTable JS 1.10.19 -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js" charset="utf8" defer></script>

    <!-- Import Toastr -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" defer>

    <script src="{{ asset('js/request.js?v='.config('app.version')) }}" defer></script>
    <script src="{{ asset('js/forms.js?v='.config('app.version')) }}" defer></script>
    <script src="{{ asset('js/app.js?v='.config('app.version')) }}" defer></script>

</body>
</html>