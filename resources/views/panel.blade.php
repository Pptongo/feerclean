@extends('layouts.master')
@section('title', \Lang::get('general.lblPanel'))
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
        <a href="#" class="navbar-brand">
            <img src="/images/logo.png" class="img-fluid" width="150" height="150" alt="Feerclean">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarNav" class="collapse navbar-collapse">
            
        </div>
        <div class="d-none d-lg-block">
            <div class="btn-group" role="group">
                <a href="/logout" class="btn btn-sm btn-danger">@lang('general.btnLogout')</a>
            </div>
        </div>
    </nav>
@endsection
@section('content')

@endsection
@section('footer')

@endsection