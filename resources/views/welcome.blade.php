@extends('layouts.master')
@section('title', \Lang::get('general.lblHome'))
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
        <a href="#" class="navbar-brand">
            <img src="/images/logo.png" class="img-fluid" width="150" height="150" alt="Feerclean">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarNav" class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">@lang('general.mnuHowItsWorks')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">@lang('general.mnuPricing')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">@lang('general.mnuLocations')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">@lang('general.mnuFaq')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">@lang('general.mnuContact')</a>
                </li>
            </ul>
        </div>
        <div class="d-none d-lg-block">
            <div class="btn-group" role="group">
                <a href="#login" class="btn btn-sm btn-success" data-smooth-scroll>@lang('general.btnLogin')</a>
                <a href="#register" class="btn btn-sm btn-info" data-smooth-scroll>@lang('general.btnRegister')</a>
            </div>
        </div>
    </nav>
@endsection
@section('content')
    <section id="home">
        <div class="content">
            <div id="login">
                <img src="/images/logo.png" class="img-fluid" width="250" height="250" alt="Feerclean">
                <form id="frmLogin" action="/login" method="POST" data-onsubmit="login">
                    <div class="form-group">
                        <label for="email" class="form-label">@lang('general.email')</label>
                        <input type="email" name="email" class="form-control" data-frm-validate="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password">@lang('general.password')</label>
                        <input type="password" name="password" class="form-control" data-frm-validate="alfanumeric" required>
                    </div>
                    <button class="btn btn-success w-100" type="button" data-submit="frmLogin" data-on-response="validateLogin">@lang('general.btnLogin')</button>
                    <p class="text-center mt-2 mb-0"><a href="#">@lang('general.forgotPassword')</a></p>
                </form>
            </div>
        </div>
    </section>
    <section id="register">
        <div class="content">
            <div class="row">
                <div class="col-lg d-flex align-items-center mb-3">
                    <div class="w-100">
                        <h1 class="text-center text-white col-md-12 col-lg-10 m-auto">@lang('general.lblRegister')</h1>
                    </div>
                </div>
                <div class="col-lg card">
                    <div class="card-body">
                        <form id="frmRegister" action="/register" method="POST" data-onsubmit="register">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="form-label required">@lang('general.lblName')</label>
                                        <input type="text" name="name" class="form-control" data-frm-validate="name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lastname" class="form-label required">@lang('general.lblLastname')</label>
                                        <input type="text" name="lastname" class="form-control" data-frm-validate="name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="form-label required">@lang('general.email')</label>
                                        <input type="email" name="email" class="form-control" data-frm-validate="email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email_confirmation" class="form-label required">@lang('general.lblEmailConfirm')</label>
                                        <input type="email" name="email_confirmation" class="form-control" data-frm-validate="email" data-frm-confirm="email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone" class="form-label">@lang('general.lblPhone')</label>
                                        <input type="tel" name="phone" class="form-control" data-frm-validate="phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile" class="form-label required">@lang('general.lblMobile')</label>
                                        <input type="tel" name="mobile" class="form-control" data-frm-validate="phone" required>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary w-md-100" type="button" data-submit="frmRegister" data-on-response="validateLogin">@lang('general.btnRegister')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <section id="footer-contact" class="p-4 bg-secondary text-light">
        <h1 class="h3 text-center">@lang('general.lblSubscribe')</h1>
        <div class="row d-flex justify-content-center mt-3">
            <form class="form-inline col-xl-6">
                <input type="email" class="form-control col-sm-12 col-md-9 mb-2" data-frm-validate="email" placeholder="@lang('general.email')">
                <div class="d-flex justify-content-center col-sm-12 col-md-3 p-0 mb-2">
                    <button type="button" class="btn btn-danger col-sm-12 col-md-11">@lang('general.btnSubscribe')</button>
                </div>
            </form>
        </div>
    </section>
    <footer class="bg-dark text-light">
        <div class="content">
            <div class="row">
                <div class="col-sm-12 col-md-4 p-5">
                    <img src="/images/logo.png" class="img-fluid col-10" width="250" height="250" alt="logo">
                </div>
                <div class="col-sm-12 col-md-4 p-5">
                    <p class="text-uppercase">@lang('general.lblMenu')</p>
                    <hr class="text-light">
                    <ul class="nav flex-column">
                        <li clas="nav-item"><a href="#" class="text-light">@lang('general.mnuHowItsWorks')</a></li>
                        <li clas="nav-item"><a href="#" class="text-light">@lang('general.mnuPricing')</a></li>
                        <li clas="nav-item"><a href="#" class="text-light">@lang('general.mnuLocations')</a></li>
                        <li clas="nav-item"><a href="#" class="text-light">@lang('general.mnuFaq')</a></li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-4 p-5">
                    <p class="text-uppercase">@lang('general.lblContact')</p>
                    <hr class="text-light">
                    <ul class="nav flex-column">
                        <li class="nav-item"><i class="fa fa-mobile-alt"></i><a href="tel:+5215512345678" class="text-light ml-3">+52 1 55 1234 5678</a></li>
                        <li class="nav-item"><i class="fa fa-envelope"></i><a href="mailto:contacto@feerclean.com" class="text-light ml-3">correo@feerclean.com</a></li>
                        <li class="nav-item"><i class="fa fa-map-marker-alt"></i><a href="https://www.google.com.mx/maps/place/FEERCLEAN/@19.676781,-99.026951,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1f2886fbac867:0x3f375be891bf63e5!8m2!3d19.676776!4d-99.0247623" target="_blank" class="text-light ml-3">Calle Capilla Mz. 72 Lt. 16, Hacienda Ojo de Agua, Ojo de Agua, 55770 Tecamac Estado de Mexico, Méx.</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
@endsection