<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::group(['middleware' => 'user_guest'], function() {
    Route::get('/', function(Request $req) {
        session(['tmpRegister' => false]);
        session(['tmpProfile' => false]);
        
        if(Auth::guard('user_login')->check()) {
            $p = '/panel';
            $q = $req->query();

            if(count($q) > 0) {
                $p .= '?';

                foreach($q as $k => $v) {
                    $p .= $k.'='.$v.'&';
                }

                $p = substr($p, 0, -1);
            }

            return redirect($p);
        }

        return view('welcome');
    });

    Route::post('/login', 'LoginController@login');
    Route::post('/register', 'RegisterController@registerUser');
    Route::get('/users/confirmRegister/{token}', 'RegisterController@confirmUser');
});

Route::group(['middleware' => 'user_logged'], function() {
    Route::get('/panel', 'LoginController@index')->name('profile');
    Route::get('/logout', 'LoginController@logout');
});