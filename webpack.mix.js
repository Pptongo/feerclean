const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('./resources/sass/app.scss', 'public/css');

mix.babel('./resources/js/app.js', 'public/js/app.js')
    .babel('./resources/js/request.js', 'public/js/request.js')
    .babel('./resources/js/forms.js', 'public/js/forms.js');